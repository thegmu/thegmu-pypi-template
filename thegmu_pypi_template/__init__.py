# -*- coding: utf-8 -*-
"""
The GMU template for every Python project that is compliant to push to PyPi.
"""

__version__ = "1.3.0"
