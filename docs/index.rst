Welcome to The GMU PYPI Template 
================================

.. image:: https://www.thegmu.com/jaz/static/img/birdie_logo_64x96.png

:mod:`thegmu-pypi-template` is a general Python project layout to create a PYPI project and is used for all PYPI Python projects at The Gregg & Mybrid Upgrade, Inc. (The GMU). The Makefile provided can immediately publish a package file to https://pypi.org, assuming you have an account. 

- Documentation: http://the-gmu-pypi-template.readthedocs.org/
- Source Code: https://bitbucket.org/thegmu/thegmu-pypi-template
- Download: https://pypi.python.org/pypi/thegmu-pypi-template

Please feel free to ask questions via email:
(mybrid@thegmu.com)

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   ../README
   thegmu_pypi_template
   sphinx_notes


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
