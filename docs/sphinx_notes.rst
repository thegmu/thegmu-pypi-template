
Sphinx Notes
=================================================

Sphinx automatic document generation from Python source files requires steps beyond the documentation on the Sphinx web site when storing documents in the "docs" directory.

Sphinx Inintialization
======================

This package downloads 12MB of documentation specific to this *thegmu-pypi-template* package for comparison and debugging purposes. Initializing the Sphinx documents for your project is best accomplished by first backing up this package documentation and then initializing from a clean "docs" directory. Once you are on solid ground updating the Sphinx documents then just delete the backup directory. 

::

 # Backup The GMU PYPI Template in directory 'docs'.
 make backup-docs
 # Remove all files from directory 'docs'.
 make destroy-docs
 # Initialize for your project. You will need the following information:
 # Project Name:
 # Author Names:
 # Version: 0.1.0
 make docs
 # Below are wizard responses to the prompts expected by the Makefile.
 # Different answers will most likely work but have not been test.
  
 Please enter values for the following settings (just press Enter to
 accept a default value, if one is given in brackets).
 
 Selected root path: .
 
 You have two options for placing the build directory for Sphinx output.
 Either, you use a directory "_build" within the root path, or you separate
 "source" and "build" directories within the root path.
 > Separate source and build directories (y/n) [n]: 

 Inside the root directory, two more directories will be created; "_templates"
 for custom HTML templates and "_static" for custom stylesheets and other static
 files. You can enter another prefix (such as ".") to replace the underscore.
 > Name prefix for templates and static dir [_]: 

 The project name will occur in several places in the built documentation.
 > Project name: <--- YOUR PROJECT NAME
 > Author name(s): <--- AUTHOR NAME
 > Project release []: <--- VERSION NUMBER, i.e. 0.1.0

 If the documents are to be written in a language other than English,
 you can select a language here by its language code. Sphinx will then
 translate text that it generates into that language.

 For a list of supported codes, see
 http://sphinx-doc.org/config.html#confval-language.
 > Project language [en]

 The file name suffix for source files. Commonly, this is either ".txt"
 or ".rst".  Only files with this suffix are considered documents.
 > Source file suffix [.rst]:

 One document is special in that it is considered the top node of the
 "contents tree", that is, it is the root of the hierarchical structure
 of the documents. Normally, this is "index", but if your "index"
 document is a custom template, you can also set this to another filename.
 > Name of your master document (without suffix) [index]: index
 Indicate which of the following Sphinx extensions should be enabled:
 > autodoc: automatically insert docstrings from modules (y/n) [n]: y
 > doctest: automatically test code snippets in doctest blocks (y/n) [n]: y
 > intersphinx: link between Sphinx documentation of different projects (y/n) [n]: y
 > todo: write "todo" entries that can be shown or hidden on build (y/n) [n]: y
 > coverage: checks for documentation coverage (y/n) [n]: y
 > imgmath: include math, rendered as PNG or SVG images (y/n) [n]: n
 > mathjax: include math, rendered in the browser by MathJax (y/n) [n]: n
 > ifconfig: conditional inclusion of content based on config values (y/n) [n]: n
 > viewcode: include links to the source code of documented Python objects (y/n) [n]: y
 > githubpages: create .nojekyll file to publish the document on GitHub pages (y/n) [n]: n
 
 A Makefile and a Windows command file can be generated for you so that you
 only have to run e.g. `make html' instead of invoking sphinx-build
 directly.
 > Create Makefile? (y/n) [y]: 
 > Create Windows command file? (y/n) [y]: n
 
 Creating file ./conf.py.
 Creating file ./index.rst.
 Creating file ./Makefile.
 
 Finished: An initial directory structure has been created.


Sphnix Expectations
===================

Sphinx documentation will most likely not be satisfactory when using the automatic generation tools. The 'Makefile' attempts to alleviate some of this by making the follow changes:

#. **Read The Docs**: Installs and configures the 'Read The Docs' template.
#. **Import source path**: Sphinx assumes it runs at the source root, but the documents are in a sub-directory, "docs". In order to auto-discover source files then the path ".." needs to be added to the Python "sys.path" in "conf.py". The Makefile adds ".." to the sys.path in the "conf.py" file. 
#. **Initial Python source doc generation**: The Sphinx automatic generation of class and module documentation from doc strings needs to be run manually the first time. The Sphinx wizard leaves one the with an impression that just answering 'yes' to a wizard prompt is good enough. It is not, a manual run is required. However, after the initial generation of source file docs then these files will be automatically updated on source documentation changes on subsequent document builds with "make html".
#. **Excludes modules.rst**: There is a directive in the index.rst file that includes the modules.rst file, "\*:\`ref:modindex\`". This makes including the "modules.rst" file redundant and and so that file is added to the 'conf.py', exclude list.

The only files that need to be manually edited are the ReST (.rst) files in the docs, top-level directory, for example:

::

 docs/index.rst	docs/modules.rst  docs/thegmu_pypi_template.rst

If new files are added then the file names with '.rst.' need to be list in the 'index.rst' file.

Sphinx White Space
==================

The Sphinx documentation syntax is white space sensitive. 

#. The number of spaces for indentation when continuing a block must be exact.
#. Extra new lines after a block are not allowed and the block continuation lines will not be read.


